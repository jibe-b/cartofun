def see_code(module, display=True):

    if display == True:
        module_file = module.__file__

        from IPython import display
        display.display(display.HTML("<a href=\"{}\">{}</a>".format(module_file, module_file)))

        cmd = "cat {} | pygmentize -l python".format(module_file, module_file)
        return get_ipython().system(cmd)

    else:
        return None
