// ==UserScript==
// @name     Unnamed Script 952247
// @version  1
// @match    https://cartofun-youplaboum.gogocarto.fr/admin/app/*
// ==/UserScript==

const base_url = "https://cartofun-youplaboum.gogocarto.fr/admin/app/"

if (localStorage.getItem("delay") === null){
  var delay = prompt("Nombre de minutes entre deux mises à jour de l'import dynamique")
  localStorage.setItem("delay", delay)
  } else { delay = localStorage.getItem("delay") }

if (window.location.href === base_url + "importdynamic/list"){
  console.log("Reloading every " + delay.toString() + " minutes.")
  window.setTimeout(
    function (){
      document.querySelectorAll("a.btn.btn-sm.btn-primary.view_link")[0].click()
    }
    , delay * 60 * 1000)
} else if (window.location.href === base_url + "importdynamic/1/refresh"){
  window.setTimeout(
    function (){
      document.querySelectorAll("button.btn.btn-default")[1].click()
    }, 2000)
}
