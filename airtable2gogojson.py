from requests import get, post
from json import loads, dump
from subprocess import run

def get_api_key():
    return open("airtable_api_key", 'r').read()[:-1]

def get_airtable_data(airtable_api_key):
    airtable_response = get(
        "https://api.airtable.com/v0/apptQTA2QSzZmisQT/referencement",
        headers={"Authorization": "Bearer {}".format(airtable_api_key)}
    )
    return loads(airtable_response.content)

def convert_to_gogocarto_schema(airtable_json_data):
    
    datapoints = []
    for item in airtable_json_data["records"]:
        item_following_gogocarto_schema = {}
        for key in item["fields"].keys():
            item_following_gogocarto_schema[key] = item["fields"][key]
        datapoints.append(item_following_gogocarto_schema)

    # TEMPORARY
    
    datapoints_patched = []
    for datapoint in datapoints:
        datapoint["geo"] = {'latitude': 48.584614, 'longitude': 7.7507127},
        datapoints_patched.append(datapoint)

    datapoints = datapoints_patched

    data_following_gogocarto_schema = {
        'licence': 'https://opendatacommons.org/licenses/odbl/summary/',
         'ontology': 'gogofull',
         'data': datapoints
        }
    
    return data_following_gogocarto_schema

def dump_to_file(data_following_gogocarto_schema):
    with open("output/data_following_gogocarto_schema.json", 'w') as f:
        dump(data_following_gogocarto_schema, f)
    
def git_add_commit():
    run(["git", "add", "output/data_following_gogocarto_schema.json"])
    run(["git", "commit", "-m", "Met à disposition les données stockées dans airtable pour un import dynamique par GogoCarto"])
    # print("Maintenant, mettez les données en ligne en exécutant: git push origin master")
    
if __name__ == "__main__":
    airtable_api_key = get_api_key()
    airtable_json_data = get_airtable_data(airtable_api_key)
    data_following_gogocarto_schema = convert_to_gogocarto_schema(airtable_json_data)
    dump_to_file(data_following_gogocarto_schema)
    git_add_commit()
    run(["git", "push", "origin", "master"])