Exécutez le notebook pour explorer les étapes à réaliser : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jibe-b%2Fcartofun/master?urlpath=tree%2Flab%2Findex.ipynb)

Ou stockez votre clef dans le fichier airtable_api_key et exécutez :

```python
python airtable2gogojson.py
```

Puis rendez le 
```
git add output/data_following_gogocarto_schema.json
git commit -m "Met à jour les données avec le schéma gogocarto"
git push origin master
```

Si vous travaillez sur un fork, allez sur [raw.githack.com](https://raw.githack.com) pour y gérer l'url `https://gitlab.com/<YOUR_NAMESPACE>/cartofun/raw/master/output/data_following_gogocarto_schema.json`

Enfin, créez un import dynamique dans un de vos projets gogocarto, qui pointe vers l'url retournée par githack.

Pour que l'exécution de l'import soit réalisée de manière plus régulière que les X jours proposés par gogocarto,

ouvrez le fichier [update_import.js](update_import.js) et copiez son contenu dans le presse-papier.

Dans votre navigateur, sur la page de l'import dynamique, pressez F12 pour ouvrir la console. Collez le script dans la console et pressez entrée.

Une popup vous demandera d'indiquer la régularité à laquelle le script exécutera à nouveau l'import.

Gardez l'onglet ouvert et votre ordinateur allumé pour permettre au script de s'exécuter.

Pour interrompre le script, cliquez sur le bouton "ok" de la popup ou fermez l'onglet.
